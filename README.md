# SEKI Climbing Areas

A map with links for the climbing areas listed on [SEKIclimbing.com](http://www.sekiclimbing.com).

## use

It's published under the [Unlicense](https://choosealicense.com/licenses/unlicense/);
you're free to use it for anything.

You can link to the [latest version](https://standardtopo.gitlab.io/seki-map/),
copy the [source](./index.html), 
or embed it as an iframe:

```
<iframe width="1000" height="500" src="https://standardtopo.gitlab.io/seki-map/"></iframe>
```
